# Toolchain Installation

The compile toolchain translates C and PDP11 assembly code to binaries in pdp11-aout format. All recent versions of binutils and gcc still support pdp11-aout as a target architecture.

## Standard Tools Required for Compilation
* GNU Binutils
    * Tested Version: 2.30.0
* GNU Compiler Collection
    * Tested Version: 8.1.0

## Binutils Installation
Download bintuils from [the GNU FTP mirrors](https://ftpmirror.gnu.org/binutils). Version 2.30.0 is known to work but others may as well. Extract to a local directory (for example `binutils`) and build with the following commands:
```bash
$ cd bintuils
$ mkdir build-pdp11-aout && cd pdp-11-aout
$ ../configure --target=pdp11-aout --prefix=/usr/local --disable-werror
$ make
$ sudo make install
```
This will build and install binutils for the pdp11-aout architecture. The `--disable-werror` option turns off the features in recent versions of gcc which treat many types of warnings as compile errors. The prefix can be changed to something other than `/usr/local` to change the install location.

The installed tools retain the same names as their x86(-64) analogues, with the prefix `pdp11-aout`.

## GCC Installation
Download the gcc sources from [the GCC FTP server](ftp://ftp.gnu.org/gnu/gcc/) or one of its [mirrors](https://www.gnu.org/prep/ftp.html). Extract to a local directory (for example `gcc`) and build with the following commands:
```bash
$ cd gcc
$ mkdir build-pdp11-aout && cd pdp11-aout
$ ../configure --target=pdp11-aout --prefix=/usr/local --disable-werror --enable-languages=c --disable-libssp --disable-libgfortran --disable-libobjc
$ make
$ sudo make install
```
This compilation takes a while. It can be sped up a bit by compiling on multiple threads (4 for example):
```bash
$ make -j4
```
The `--disable-xxxx` flags turn off several libraries that will not compile under the pdp11-aout architecture. 

These instructions were adapted from [the Clarkson CSLabs Wiki](http://docs.cslabs.clarkson.edu/wiki/Developing_for_a_PDP-11). 
