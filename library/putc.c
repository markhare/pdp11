/* PUTC: Write character to PDP11 console
 * 
 * INPUTS:
 * =======
 * 	char character: The character to write
 *
 * OUTPUTS:
 * ========
 *	int error: Error code. 0 if success, 2, if timeout.
 */

#include "putc.h"

#define XCSR 0177564
#define XBUF 0177566
#define TXRDY 0x0080
#define NRETRY 5

int
putc(char character)
{
	const unsigned short int *xcsr = (unsigned short int *)XCSR;
	char *xbuf = (char *)XBUF;
	int tries = 0;
	int error = 0;

	while((*xcsr & TXRDY) == 0)
	{
		tries++;
		if (tries > NRETRY)
		{
			/* Error 2: Too many tries */
			error = 2;
			break;
		}
	}
	
	/* Write character to console */
	*xbuf = character;

	return error;
}
