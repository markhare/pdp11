/* PUTS: Write string to PDP11 console
 *
 * INPUTS:
 * =======
 *  char *message: Pointer to null-terminated string
 * 
 * OUTPUTS:
 * ========
 *  int error: Error code. 0 if success, 2 if timeout.
 */
#include "puts.h"
#include "putc.h"

int
puts(char *message)
{
    int error = 0;
    int i = 0;

    while(message[i] != '\0')
    {
        error = putc(message[i]);
        if(error)
        {
            break;
        }
        i++;
    }

    return error;
}