/* ITOA: Convert integer to character string
 *
 * INPUTS:
 * =======
 *	int num: Number to convert
 *	char *str: Pointer to character array output
 *	int base: Number system base
 *
 * OUTPUTS:
 * ========
 *	Converted string in *str
 *
 * TODO:
 * =====
 *	Fix multiple returns
 */

#include "itoa.h"

void
itoa(int num, char *str, int base)
{
	int i = 0;
	int isNegative = 0;
	int rem;
	int done = 0;

	/* Handle 0: */
	if (num == 0)
	{
		str[i++] = '0';
		str[i] = '\0';
		done = 1;
	}

	/* Otherwise, not done: */
	if (!done)
	{
		/* Negative numbers only handled in base 10 */
		if (num < 0 && base == 10)
		{
			isNegative = 1;
			num = -num;
		}

		while (num != 0)
		{
			rem = num % base;
			str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
			/* Integer Division: */
			num = num / base;
		}

		if (isNegative)
		{
			str[i++] = '-';
		}

		/* Terminator */
		str[i] = '\0';

		/* Reverse the string */
		strrev(str);
	}

	return str;
}

/* STRREV: Reverse String
 *
 * INPUTS:
 * =======
 *	char *p: Pointer to null-terminated string to reverse
 *
 * OUTPUTS:
 * ========
 * 	String reversed in place
 */
void
strrev(char *p)
{
	char *q = p;
	while (q && *q)
	{
		++q;
	}
	for (--q; p < q; ++p, --q)
	{
		*p = *p ^ *q;
		*q = *p ^ *q;
		*p = *p ^ *q;
	}
}
