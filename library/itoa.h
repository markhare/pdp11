#ifndef _ITOA_H
#define _ITOA_H

void itoa(int num, char *str, int base);
void strrev(char *p);

#endif /* _ITOA_H */
