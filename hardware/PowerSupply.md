# Power Supply Troubleshooting

## Power Supplies
Three power supplies are visible at the bottom rear of the CPU case:

![Power Supplies Illustration](power_supply.png)

1. 5V 32A Supply. LED on when power on.
2. 5V 32A Supply. LED does not currently power on. Suspect this power supply to be the issue.
3. 15V 10A Supply. Incandescent light burnt out, but power is present when unit is on.

More photos are available in the `power_supply_photos` directory.

## Log
* 2018-06-22: Attempted to power on CPU for the first time since the move. Fans came on and front panel LEDs began to light, then faded. Fans continued to run. 28V power was observed at the power switch but 5V power to the front panel was missing. Traced back to one of three power supplies (see below).

* 2018-06-25: Removed all three power supplies. Steps:
    * Removing the top from the power unit (loosen three screws on top, remove four screws from the back.) 
    * Rotate BA11-K chassis 90 degrees to vertical. Detach wire harnesses and remove the screws holding the power supply into the chassis. Be careful, as one of the ground wires will still be attached. This wire can be detached or the power supply can be rested on a base under the chassis.
    * Remove two wide round-headed machine screws from the front (inside) of the power supply and one small round-headed machine screw from the back (outside) of the power supply for each power regulator.
    * The power regulators can then be removed from the top of the power supply. See photos below:
    
    <img src="power_supply_photos/ps_overview.jpg" width="350" />
    <img src="power_supply_photos/H7441_overview.jpg" width="350" />

    * The fuse (labelled `BUSS AGC 15A 32V`) within PS2 was found to be broken (see [photo](power_supply_photos/fuse.jpg) directory). Fuse removed.
    * The power regulator appeared to be in otherwise good condition.
* 2018-06-26:
    * Replaced fuse in PS2 with slow-blow 15A AGC fuse. Replaced power supplies and attempted to power on. 
    * LED on PS2 lit briefly and went out. Suspect fuse blown.
    * Check output for proper voltage, check for damaged components. Suspect crowbar circuit is activating for some reason.
    
    <img src="power_supply_photos/circuit_diagram.png" width="500" />