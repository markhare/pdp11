# PDP11GUI Information

Licensed under MIT License (included in this folder). Runs on windows, seems to work well with WINE (or PlayOnLinux). Must connect WINE's COM1 to Linux /dev/ttyUSBx for USB -> Serial adapter.

## Websites

https://github.com/j-hoppe/PDP11GUI

http://retrocmp.com/tools/pdp11gui

## Installer

The installer and source included in this folder are V 1.48.6, released 2016-10-31.
