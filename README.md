# PDP-11 Information

Mark D. Hare

markdanielhare@gmail.com

## Introduction
Information for programming and running PDP11/34 software and hardware.

## Toolchain
This directory contains steps to assemble a working toolchain for compiling C and assembly programs into a raw binary in PDP11 format.

[Installation Instructions](toolchain/Install.md)

## Library
C and assembly functions for input/output, math, etc.

## Programs
Full programs ready to be compiled into memory images.

## Tools
Tools for converting raw binaries to memory images, tape images, as well as communication software and external programs.

## Hardware
Information about the hardware PDP11/34a.

### New Hardware
* BERG-40 to DB-9 Serial Adapter Board (custom design)

### Recent Maintenance
* [Power Supply Troubleshooting](hardware/PowerSupply.md)

## Miscellaneous Links

http://ancientbits.blogspot.com/2012/07/programming-barebones-pdp11.html

https://virtuallyfun.com/wordpress/category/pdp11/

https://github.com/dwelch67/pdp11_samples

http://docs.cslabs.clarkson.edu/wiki/Developing_for_a_PDP-11
